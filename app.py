import logging

import fire

from plugins import TokenPriceChartPlugin, TokenPriceQueryPlugin
from xiaowing.bot import XiaowingBot


class Application:
    def __init__(self):
        self._bot = XiaowingBot(
            [TokenPriceChartPlugin(), TokenPriceQueryPlugin()], '0.0.0.0', 80, '34.217.95.103')    # for server
        #[TokenPriceChartPlugin(), TokenPriceQueryPlugin()], '0.0.0.0', 8080, '127.0.0.1')    # for local

    def run(self):
        self._bot.run()


if __name__ == "__main__":
    logging.basicConfig(
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
    fire.Fire(Application)
