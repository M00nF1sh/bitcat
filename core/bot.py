from abc import ABCMeta, abstractmethod
from typing import Collection

from .messages import GroupMessage
from .plugin import Plugin


class WechatBot(metaclass=ABCMeta):
    def __init__(self, plugins: Collection[Plugin]):
        self.plugins = plugins

    @abstractmethod
    def run(self):
        pass

    async def on_group_message(self, message: GroupMessage):
        for plugin in self.plugins:
            await plugin.on_group_message(message)
