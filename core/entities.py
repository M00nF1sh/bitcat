
class ChatUser:
    def __init__(self, nickname, display_name):
        self.nickname = nickname
        self.display_name = display_name


class ChatGroup:
    def __init__(self, id, name):
        self.id = id
        self.name = name
