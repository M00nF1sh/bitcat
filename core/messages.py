import json
from .entities import ChatUser, ChatGroup
from .types import InMemoryFile
from abc import ABCMeta, abstractmethod


class GroupMessage(metaclass=ABCMeta):
    def __init__(self, group: ChatGroup, user: ChatUser, content: str):
        self.group = group
        self.user = user
        self.content = content

    @abstractmethod
    def reply_text(self, text):
        pass

    @abstractmethod
    def reply_image(self, image: InMemoryFile):
        pass

    def __str__(self):
        return json.dumps({
            'content': self.content
        })