from abc import ABCMeta, abstractmethod

from .messages import GroupMessage


class Plugin:
    def __init__(self):
        pass

    @abstractmethod
    async def on_group_message(self, message: GroupMessage):
        pass
