class InMemoryFile:
    def __init__(self, payload: bytes, file_format: str):
        self.payload = payload
        self.format = file_format
