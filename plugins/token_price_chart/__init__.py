from core import Plugin, GroupMessage
from .ohlcv_data_loader import OHLCVDataLoader
from .ohlcv_drawer import OHLCVDrawer

import re


class TokenPriceChartPlugin(Plugin):
    def __init__(self):
        self.data_loader = OHLCVDataLoader()
        self.drawer = OHLCVDrawer()
        self.chart_query_pattern = re.compile(r'^([a-zA-Z]{2,4})图$')

    async def on_group_message(self, message: GroupMessage):
        token = self._extract_token(message.content)
        if token:
            quote_currency, ohlvc_data = await self.data_loader.load(token)
            if quote_currency:
                image = await self.drawer.draw(token, quote_currency, ohlvc_data)
                if image:
                    message.reply_image(image)

    def _extract_token(self, content):
        matches = self.chart_query_pattern.match(content)
        if matches:
            return matches.group(1).upper()
        return None
