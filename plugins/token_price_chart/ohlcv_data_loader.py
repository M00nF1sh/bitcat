import time

import ccxt.async as ccxt
import numpy as np
from aiocache import cached


class OHLCVDataLoader:
    def __init__(self):
        self.exchanges = [
            ccxt.binance(),
            ccxt.huobipro(),
            ccxt.bitfinex(),
            ccxt.okex(),
            ccxt.zb(),
            ccxt.gateio(),
            ccxt.bitz()]
        self.timeframe = '15m'
        self.quote_currencies = ['USDT', 'BTC', 'ETH']

    @cached(ttl=60)
    async def load(self, token):
        for quote_currency in self.quote_currencies:
            token_exchange_mapping = await self._build_token_exchange_mapping(quote_currency)
            exchange = token_exchange_mapping.get(token, None)
            if exchange:
                yesterday = int((time.time() - 24 * 60 * 60) * 1000)
                symbol = "{base}/{quote}".format(base=token,
                                                 quote=quote_currency)
                ohlcv_data = await exchange.fetch_ohlcv(symbol, timeframe=self.timeframe, since=yesterday)
                ohlcv_data = np.array(ohlcv_data).astype(np.float)
                return quote_currency, ohlcv_data
        return None, None

    @cached(ttl=24*60*60)
    async def _build_token_exchange_mapping(self, quote_currency):
        token_exchange_mapping = {}
        for exchange in self.exchanges:
            for market in await exchange.fetch_markets():
                token = market['base']
                quote = market['quote']
                if (token not in token_exchange_mapping) and (quote == quote_currency):
                    token_exchange_mapping[token] = exchange
        return token_exchange_mapping
