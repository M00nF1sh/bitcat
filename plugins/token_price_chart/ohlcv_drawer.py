import asyncio
import io
import os
import numpy as np
import matplotlib
matplotlib.use('Agg')   # important! must be put here
import matplotlib.dates as mdate
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
import matplotlib.image as image
import matplotlib.font_manager as fm
from matplotlib.pylab import epoch2num
import mpl_finance as mpf
from core.types import InMemoryFile


class OHLCVDrawer:
    """
    Draws 
    """

    def __init__(self):
        self.image_format = 'png'
        self.label_font = self._load_label_font()
        self.chain_love_logo = self._load_chain_love_logo()

    async def draw(self, token, quote_currency, ohlcv_data)->InMemoryFile:
        drawing_data = self._build_drawing_data(ohlcv_data)

        plt.rcParams['font.sans-serif'] = ['sans-serif']
        plt.rcParams['axes.unicode_minus'] = False
        plt.rcParams['timezone'] = 'Asia/Shanghai'

        fig = plt.figure()
        axis = plt.subplot()
        #plt.subplots_adjust(bottom=0.28)
        mpf.candlestick_ochl(axis, drawing_data, width=0.005,
                             colorup='g', colordown='r', alpha=1.0)
        plt.grid(True)
        axis.xaxis.set_major_formatter(mdate.DateFormatter('%H:%M'))
        axis.set_title(
            '{}/{}-最近24小时 (区块链爱 & 比特喵报价机器人)'.format(token, quote_currency),
            fontproperties=self.label_font, fontsize=13)
        axis.set_ylabel('价格', fontproperties=self.label_font)
        axis.grid(True)

        #fig.figimage(self.chain_love_logo, 360, 0, zorder=3)

        buffer = io.BytesIO()
        plt.savefig(buffer, format=self.image_format)
        plt.close(fig)
        return InMemoryFile(buffer.getvalue(), self.image_format)

    def _load_chain_love_logo(self):
        logo_path = os.path.join(os.getcwd(), 'assets/img/chain_love.png')
        datafile = cbook.get_sample_data(logo_path, asfileobj=False)
        logo = image.imread(datafile)
        logo.flags.writeable = True
        return logo

    def _load_label_font(self):
        font_path = os.path.join(os.getcwd(), 'assets/font/jijianjihe.ttf')
        return fm.FontProperties(fname=font_path)

    def _build_drawing_data(self, ohlcv_data):
        drawing_data = np.copy(ohlcv_data)
        num_time = [epoch2num(timestamp / 1000)
                    for timestamp in drawing_data[:, 0]]
        drawing_data[:, 0] = num_time
        drawing_data[:, [2, 4]] = drawing_data[:, [4, 2]]
        return drawing_data
