from core import Plugin, GroupMessage
import re
from .price_data_loader import PriceDataLoaderChain
from .coinmarketcap_loader import CoinmarketcapLoader
from .ccxt_loader import CCXTLoader
from .price_data_formatter import PriceDataFormatter


class TokenPriceQueryPlugin(Plugin):
    def __init__(self):
        self.chart_query_pattern = re.compile(r'^([a-zA-Z]{2,4})$')
        self.data_loader = PriceDataLoaderChain(
            CoinmarketcapLoader(), CCXTLoader())
        self.data_formatter = PriceDataFormatter()

    async def on_group_message(self, message: GroupMessage):
        token = self._extract_token(message.content)
        price_tick = await self.data_loader.load(token)
        if price_tick:
            price_info = await self.data_formatter.format(token, price_tick)
            message.reply_text(price_info)

    def _extract_token(self, content):
        matches = self.chart_query_pattern.match(content)
        if matches:
            return matches.group(1).upper()
        return None
