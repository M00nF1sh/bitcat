import time

import ccxt.async as ccxt
from utils import optional_map
import numpy as np
from aiocache import cached
from .price_data_loader import PriceDataLoader, PriceTick


class CCXTLoader(PriceDataLoader):
    def __init__(self):
        self.exchanges = [
            ccxt.binance(),
            ccxt.huobipro(),
            ccxt.bitfinex(),
            ccxt.okex(),
            ccxt.zb(),
            ccxt.gateio(),
            ccxt.bitz()]
        self.quote_currencies = ['USDT', 'BTC', 'ETH']

    @cached(ttl=60)
    async def load(self, token)->PriceTick:
        for quote_currency in self.quote_currencies:
            token_exchange_mapping = await self._build_token_exchange_mapping(quote_currency)
            exchange = token_exchange_mapping.get(token, None)
            if exchange:
                ticker = await exchange.fetch_ticker('{}/{}'.format(token, quote_currency))
                name = token
                rank = None
                percent_change_1h = None
                percent_change_24h = optional_map(
                    ticker['percentage'], lambda value: float(value) / 100)
                percent_change_7d = None
                price = optional_map(
                    ticker['last'], lambda value: float(value))
                marketcap = None
                volume_24h = optional_map(
                    ticker['quoteVolume'], lambda value: float(value))
                low_24h = optional_map(
                    ticker['low'], lambda value: float(value))
                high_24h = optional_map(
                    ticker['high'], lambda value: float(value))
                return PriceTick(name, rank,
                                 percent_change_1h, percent_change_24h, percent_change_7d,
                                 price, marketcap, volume_24h, low_24h, high_24h, quote_currency)
        return None

    @cached(ttl=24*60*60)
    async def _build_token_exchange_mapping(self, quote_currency):
        token_exchange_mapping = {}
        for exchange in self.exchanges:
            for market in await exchange.fetch_markets():
                token = market['base']
                quote = market['quote']
                if (token not in token_exchange_mapping) and (quote == quote_currency):
                    token_exchange_mapping[token] = exchange
        return token_exchange_mapping
