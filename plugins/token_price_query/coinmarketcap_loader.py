import time

import ccxt.async as ccxt
from utils import optional_map
import numpy as np
from aiocache import cached
from .price_data_loader import PriceDataLoader, PriceTick


class CoinmarketcapLoader(PriceDataLoader):
    def __init__(self):
        self.exchange = ccxt.coinmarketcap()
        self.quote_currency = 'USD'

    @cached(ttl=60)
    async def load(self, token)->PriceTick:
        supported_tokens = await self._build_supported_token_mapping()
        if token in supported_tokens:
            ticker = await self.exchange.fetch_ticker('{}/{}'.format(token, self.quote_currency))
            name = ticker['info'].get('name', None)
            rank = ticker['info'].get('rank', None)
            percent_change_1h = optional_map(
                ticker['info'].get('percent_change_1h'), lambda value: float(value) / 100)
            percent_change_24h = optional_map(
                ticker['info'].get('percent_change_24h'), lambda value: float(value) / 100)
            percent_change_7d = optional_map(
                ticker['info'].get('percent_change_7d'), lambda value: float(value) / 100)
            price_usd = optional_map(ticker['info'].get(
                'price_usd'), lambda value: float(value))
            marketcap_usd = optional_map(ticker['info'].get(
                'market_cap_usd'), lambda value: float(value))
            volume_usd_24h = optional_map(ticker['info'].get(
                '24h_volume_usd'), lambda value: float(value))
            return PriceTick(name, rank,
                             percent_change_1h, percent_change_24h, percent_change_7d,
                             price_usd, marketcap_usd, volume_usd_24h, None, None, self.quote_currency)
        return None

    @cached(ttl=24*60*60)
    async def _build_supported_token_mapping(self):
        supported_tokens = set()
        for market in await self.exchange.fetch_markets():
            token = market['base']
            quote = market['quote']
            if (token not in supported_tokens) and (quote == self.quote_currency):
                supported_tokens.add(token)
        return supported_tokens
