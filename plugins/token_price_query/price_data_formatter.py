from services.currency import DefaultExchangeService
from .price_data_loader import PriceTick


class PriceDataFormatter:
    def __init__(self):
        self.currency_exchange = DefaultExchangeService()

    async def format(self, token: str, tick: PriceTick):
        lines = ['[爱心]{}实时行情[爱心]'.format(token)]
        lines.append('▂﹏▂﹏▂﹏▂﹏▂﹏▂')

        price_usd = await self._convert_to_usd(tick.price, tick.currency)
        if price_usd:
            lines.append('[發]美元: ${:.2f}'.format(price_usd))
        price_cny = await self._convert_to_cny(tick.price, tick.currency)
        if price_cny:
            lines.append('[發]人民币: ¥{:.2f}'.format(price_cny))
        if token == 'PNT':
            if tick.currency == 'BTC':
                lines.append('[發]比特币:{:.7f}'.format(tick.price))
            low_24h = await self._convert_to_cny(tick.low_24h, tick.currency)
            high_24h = await self._convert_to_cny(tick.high_24h, tick.currency)
            if high_24h:
                lines.append('[發]24小时最高人民币:¥{:.2f}'.format(high_24h))
            if low_24h:
                lines.append('[發]24小时最低人民币:¥{:.2f}'.format(low_24h))
        if tick.percent_change_1h:
            lines.append('[發]1小时变化率: {:+.2%}'.format(tick.percent_change_1h))
        if tick.percent_change_24h:
            lines.append('[發]24小时变化率: {:+.2%}'.format(tick.percent_change_24h))
        if tick.percent_change_7d:
            lines.append('[發]7天变化率: {:+.2%}'.format(tick.percent_change_7d))

        lines.append('[發]市值排行: {}'.format(tick.rank or '无'))
        lines.append('▂﹏▂﹏▂﹏▂﹏▂﹏▂')
        lines.append('比特喵: 功能最全的报价猫')

        if token != 'PNT':
            lines.append('回复{}图试试'.format(token))
        return '\n'.join(lines)

    async def _convert_to_usd(self, amount, currency):
        if amount:
            rate = await self.currency_exchange.exchange(currency, 'USD')
            return amount * rate
        return None

    async def _convert_to_cny(self, amount, currency):
        if amount:
            rate = await self.currency_exchange.exchange(currency, 'CNY')
            return amount * rate
        return None
