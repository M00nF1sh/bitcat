from abc import ABCMeta, abstractmethod


class PriceTick:
    def __init__(self, name, rank,
                 percent_change_1h, percent_change_24h, percent_change_7d,
                 price, marketcap, volume_24h, low_24h, high_24h, currency):
        self.name = name
        self.rank = rank
        self.percent_change_1h = percent_change_1h
        self.percent_change_24h = percent_change_24h
        self.percent_change_7d = percent_change_7d
        self.price = price
        self.marketcap = marketcap
        self.volume_24h = volume_24h
        self.low_24h = low_24h
        self.high_24h = high_24h
        self.currency = currency


class PriceDataLoader(metaclass=ABCMeta):
    @abstractmethod
    async def load(self, token) -> PriceTick:
        pass


class PriceDataLoaderChain(PriceDataLoader):
    def __init__(self, *data_loaders):
        self.data_loaders = data_loaders

    async def load(self, token) -> PriceTick:
        for data_loader in self.data_loaders:
            tick = await data_loader.load(token)
            if tick:
                return tick
