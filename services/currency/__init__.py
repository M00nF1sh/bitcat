from .exchange import ExchangeService
from .crypto_exchange import CryptoExchangeService
from .fiat_exchange import FiatExchangeService


class DefaultExchangeService(ExchangeService):
    def __init__(self):
        self.fiat_exchange = FiatExchangeService()
        self.crypto_exchange = CryptoExchangeService()

    async def exchange(self, from_symbol, to_symbol):
        if from_symbol == to_symbol:
            return 1
        if from_symbol in ['USDT', 'BTC', 'ETH', 'BCH'] and to_symbol in ['USD', 'CNY']:
            return await self.crypto_exchange.exchange(from_symbol, to_symbol)
        return await self.fiat_exchange.exchange(from_symbol, to_symbol)
