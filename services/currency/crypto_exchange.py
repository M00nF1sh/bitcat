import aiohttp
import ccxt.async as ccxt
from aiocache import cached
from .exchange import ExchangeService


class CryptoExchangeService(ExchangeService):
    def __init__(self):
        self.coinmarketcap = ccxt.coinmarketcap()

    @cached(ttl=10 * 60)
    async def exchange(self, from_symbol, to_symbol):
        ticker = await self.coinmarketcap.fetch_ticker('{}/{}'.format(from_symbol, to_symbol))
        return ticker['last']
