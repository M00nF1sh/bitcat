from abc import ABCMeta, abstractmethod


class ExchangeService(metaclass=ABCMeta):
    @abstractmethod
    async def exchange(self, from_symbol, to_symbol):
        pass
