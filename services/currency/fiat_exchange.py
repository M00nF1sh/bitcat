import aiohttp
from aiocache import cached
from .exchange import ExchangeService


class FiatExchangeService(ExchangeService):
    def __init__(self):
        pass

    @cached(ttl=24 * 60 * 60)
    async def exchange(self, from_symbol, to_symbol):
        if from_symbol == 'USD' and to_symbol == 'CNY':
            return 6.4
        elif from_symbol == 'CNY' and to_symbol == 'USD':
            return 0.15625
        raise Exception("unsupported!")
