def optional_map(value, mapper):
    return mapper(value) if value else None
