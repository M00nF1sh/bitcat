import asyncio
import logging
from typing import Collection

from aiohttp import web

from core.bot import WechatBot
from core.types import InMemoryFile
from .file_manager import OnetimeFileManager


logger = logging.getLogger(__name__)

class XiaowingBot(WechatBot):
    def __init__(self, plugins, bind_address: str, bind_port: int, server_address: str):
        super().__init__(plugins)
        self.bind_address = bind_address
        self.bind_port = bind_port
        self.server_address = server_address
        self.file_manager = OnetimeFileManager()

    def run(self):
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self._async_run(loop))
        loop.run_forever()

    def serve_onetime(self, fileObj: InMemoryFile):
        filename = self.file_manager.store(fileObj)
        return "http://{}:{}/static/{}".format(self.server_address, self.bind_port, filename)

    async def _async_run(self, loop):
        app = web.Application(loop=loop)
        app.router.add_route('GET', '/ping', self._handle_ping)
        app.router.add_route(
            'GET', '/static/{filename}', self._handle_static_resource)
        app.router.add_route('POST', '/', self._handle_message)
        server = await loop.create_server(app.make_handler(), self.bind_address, self.bind_port)
        logging.info('Server started at {}:{}'.format(
            self.bind_address, self.bind_port))
        return server

    async def _handle_ping(self, request: web.Request):
        return web.Response(text="pong!")

    async def _handle_message(self, request: web.Request):
        message: XiaowingGroupMessage = await XiaowingGroupMessage.create(self, request)
        logger.info("received message:{}".format(message))
        await self.on_group_message(message)
        return message.build_response()

    async def _handle_static_resource(self, request: web.Request):
        filename = request.match_info['filename']
        in_memory_file = self.file_manager.retrieve(filename)
        if in_memory_file:
            return web.Response(body=in_memory_file.payload)
        return web.Response(status=404)


from .messages import XiaowingGroupMessage
