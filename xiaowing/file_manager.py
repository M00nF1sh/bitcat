import uuid

from aiohttp import web

from core.types import InMemoryFile


class OnetimeFileManager:
    def __init__(self):
        self.temp_files = {}

    def store(self, fileObj: InMemoryFile):
        filename = self._generate_filename(fileObj)
        self.temp_files[filename] = fileObj
        return filename

    def retrieve(self, filename)->InMemoryFile:
        return self.temp_files.pop(filename, None)

    def _generate_filename(self, fileObj: InMemoryFile):
        return "{name}.{ext}".format(name=uuid.uuid4(), ext=fileObj.format)
