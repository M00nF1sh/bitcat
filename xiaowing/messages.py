import json
import logging

from aiohttp import web

from core.entities import ChatGroup, ChatUser
from core.messages import GroupMessage

from .bot import XiaowingBot
from .utils import unicode_escape

logger = logging.getLogger(__name__)


class XiaowingGroupMessage(GroupMessage):
    @staticmethod
    async def create(bot: XiaowingBot, request: web.Request):
        params = await request.post()
        group_id = params.get('gid')
        group_name = unicode_escape(params.get('gname'))
        nickname = unicode_escape(params.get('nickname'))
        display_name = unicode_escape(params.get('displayname'))
        content = unicode_escape(params.get('content'))
        chat_user = ChatUser(nickname, display_name)
        chat_group = ChatGroup(group_id, group_name)
        return XiaowingGroupMessage(bot, chat_group, chat_user, content)

    def __init__(self, bot: XiaowingBot, group: ChatGroup, user: ChatUser, content: str):
        super().__init__(group, user, content)
        self._bot = bot
        self._text_replied = []
        self._image_replied = []

    def reply_text(self, text):
        self._text_replied.append(text)

    def reply_image(self, image):
        self._image_replied.append(image)

    def build_response(self)->web.Response:
        messages = self._text_replied
        if len(messages) == 0:
            messages.append('')
        messages.extend(['[img]{url}[/img]'.format(url=self._bot.serve_onetime(image))
                         for image in self._image_replied])
        tip = '[结束]'.join(messages)
        resp_text = '''{{"rs":1,"tip":"{tip}","end":0}}'''.format(
            tip=tip)
        logger.info(resp_text)
        return web.Response(text=resp_text)
