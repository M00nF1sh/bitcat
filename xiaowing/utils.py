def unicode_escape(payload: str)->str:
    return payload.encode('utf-8').decode('unicode_escape')
